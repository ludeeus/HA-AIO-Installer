# HA-AIO-Installer
An unofficial "AIO" installer based on Hassbian.

This script will:
- Set up HA in a venv, running as `homeassistant`
- Install hassbian-config for easy installation of additional modules.

### Prerequisites
- wget

### Run this command to install:
```bash
$ wget https://gitlab.com/ludeeus/HA-AIO-Installer/raw/master/installer.sh
$ sudo bash installer.sh
```

### Tested on:
- Debian 9
- Raspbian Strtech (minimal)
- Raspbian Stretch (w/GUI)
- Ubuntu server 18.04